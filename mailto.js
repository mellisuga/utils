import JSObfuscator from 'javascript-obfuscator';
const ObfuscateJS = JSObfuscator.obfuscate;

import nunjucks from 'nunjucks';

import path from 'path'
import fs from 'fs'

import crypto from 'crypto'

import { URL } from 'url'; // in Browser, the URL in native accessible on window
const __filename = new URL('', import.meta.url).pathname;
const __dirname = new URL('.', import.meta.url).pathname;

const mailto_js_njk = fs.readFileSync(path.resolve(__dirname, 'mailto.njk.js'), "utf8")


const AZaz = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";


function uint8_uint2(uint8) {
  const uint2 = [];
  for (let pow = 7; pow > -1; pow--) {
    const cbit_val = Math.pow(2, pow);
    if (uint8 >= cbit_val) {
      if (pow % 2 > 0) {
        uint2.push(2);
      } else {
        uint2[uint2.length-1] += 1;
      }
      uint8 -= cbit_val;
    } else {
      if (pow % 2 > 0) {
        uint2.push(0);
      }
    }
  }
  return uint2;
}

function sym_encrypt(data, secret) {
  console.log(secret);
  let uint2_secret = []
  for (let c = 0; c < secret.length; c++) {
    uint2_secret = [...uint2_secret, ...uint8_uint2(secret.charCodeAt(c))]
  }
  console.log(uint2_secret, uint2_secret.length);

  const symbols = [];
  const ck_symbol = (symbol) => {
    if (symbols.includes(symbol)) {
      return false;
    } else {
      for (const osym of symbols) {
        if (osym.includes(symbol) || symbol.includes(osym)) {
          return false;
        }
      }
      return true;
    }
  }
  for (let s = 0; s < uint2_secret.length; s++) {
    const pattern_length = 3+uint2_secret[s];
    let new_symbol = [...Array(pattern_length)].map(() => Math.floor(Math.random() * 16).toString(16)).join('');
    while (!ck_symbol(new_symbol)) {
      console.log("RESYMBOL", new_symbol);
      new_symbol = [...Array(pattern_length)].map(() => Math.floor(Math.random() * 16).toString(16)).join('');
    }
    symbols.push(new_symbol);
  }

  let encrypted_string = "";
  for (const symbol of symbols) {
    encrypted_string += symbol;
  }
  for (let c = 0; c < data.length; c++) {

    encrypted_string += symbols[data.charCodeAt(c)];
  }

  return encrypted_string;
}

export default class MailTo {
  static async construct(email, html_njk, cfg) {
    try {
      let _this = new MailTo();
      _this.output = email;

      const re_secret = crypto.randomBytes(64).toString("ascii");
      const encrypted_email = sym_encrypt(email, re_secret);
      const encrypted_mailto = sym_encrypt("mailto:"+email, re_secret);
      const encrypted_tel = sym_encrypt("tel:"+email, re_secret);





//      console.log(symbols);
      const base64_ea = btoa(_this.output);

//      console.log(ctx.email_address);






      const script_variables = [
        "sym_decrypt",
        "encrypted_string",
        "secret_identifier",
        "uint8_uint2",
        "uint8",
        "uint2",
        "pow",
        "cbit_val",
        "encrypted_string",
        "reconstructed_symbols",
        "uint2_secret",
        "c", "s",
        "uint2_secret",
        "symbol",
        "decrypted_string",
        "decrypting",
        "pattern_length",
        "found_symbol",

        "display_fc",
        "display_id",
        "display_encrypted_email",

        "encrypted_mailto",

        "copy_encrypted_email"
      ]
      const script_context = {};

      function gen_var_name() {
        let nhex = AZaz.charAt(Math.floor(Math.random() * AZaz.length))
          +crypto.randomBytes(8 + Math.round(Math.random() * 8)).toString("hex");

        while (script_context[nhex]) {
          nhex = AZaz.charAt(Math.floor(Math.random() * AZaz.length))
            +crypto.randomBytes(8 + Math.round(Math.random() * 8)).toString("hex");
        }

        AZaz.charAt(Math.floor(Math.random() * AZaz.length));
        
//        console.log("nvarname", nhex);
        return nhex;
      }

      for (const svar of script_variables) {
        script_context[svar] = gen_var_name();
      }

      script_context["display_fc"] = gen_var_name();
      script_context["mailto_fc"] = gen_var_name();
      script_context["copy_fc"] = gen_var_name();
      script_context["email_display_id"] = gen_var_name();

      script_context["secret_value"] = btoa(re_secret);
      
  //    console.log(script_context);

      const rendered_script = nunjucks.renderString(mailto_js_njk, script_context);
//      console.log(rendered_script);


      _this.output = "<script>"+rendered_script+"</script>";

/*

      _this.output = "<script>"+ObfuscateJS(rendered_script,
          {
              compact: true,
              controlFlowFlattening: true,
              controlFlowFlatteningThreshold: 1,
              numbersToExpressions: true,
              simplify: true,
              stringArrayShuffle: true,
              splitStrings: true,
              stringArrayThreshold: 1
          }
      )+"</script>";
*/

      let email_stars = '';
      for (let s = 0; s < email.length; s++) {
        email_stars+='•';
      }


      _this.output += nunjucks.renderString(html_njk, {
        email_stars: email_stars,
        display_fc: script_context["display_fc"],
        mailto_fc: script_context["mailto_fc"],
        copy_fc: script_context["copy_fc"],
        email_display_id: script_context["email_display_id"],
        encrypted_email: encrypted_email,
        encrypted_mailto: encrypted_mailto,
        encrypted_tel: encrypted_tel
      });
      
      return _this.output;
    } catch (e) {
      console.error(e.stack);
    }
  }

  constructor() {

  }
}
