

const {{ secret_identifier }} = atob("{{ secret_value }}");

function {{ uint8_uint2 }}({{ uint8 }}) {
  const {{ uint2 }} = [];
  for (let {{ pow }} = 7; {{ pow }} > -1; {{ pow }}--) {
    const {{ cbit_val }} = Math.pow(2, {{ pow }});
    if ({{ uint8 }} >= {{ cbit_val }}) {
      if ({{ pow }} % 2 > 0) {
        {{ uint2 }}.push(2);
      } else {
        {{ uint2 }}[{{ uint2 }}.length-1] += 1;
      }
      {{ uint8 }} -= {{ cbit_val }};
    } else {
      if ({{ pow }} % 2 > 0) {
        {{ uint2 }}.push(0);
      }
    }
  }
  return {{ uint2 }};
}

function {{ sym_decrypt }}({{ encrypted_string }}) {
  const {{ reconstructed_symbols }} = [];
  let {{ uint2_secret }} = []
  for (let {{ c }} = 0; {{ c }} < {{ secret_identifier }}.length; {{ c }}++) {
    {{ uint2_secret }} = [...{{ uint2_secret }}, ...{{ uint8_uint2 }}({{ secret_identifier }}.charCodeAt({{ c }}))]
  }
      console.log({{ uint2_secret }}, {{ uint2_secret }}.length);

  for (let {{ s }} = 0; {{ s }} < {{ uint2_secret }}.length; {{ s }}++) {
    const {{ pattern_length }} = 3+{{ uint2_secret }}[{{ s }}];
    {{ reconstructed_symbols }}.push({{ encrypted_string }}.slice(0, {{ pattern_length }}));
    {{ encrypted_string }} = {{ encrypted_string }}.substring({{ pattern_length }});
  }
  console.log({{ reconstructed_symbols }});

  let {{ decrypted_string }} = ""; 
  let {{ decrypting }} = true;
  while ({{ decrypting }}) {
    let {{ found_symbol }} = false;
    for (const {{ symbol }} of {{ reconstructed_symbols }}) {
      if ({{ encrypted_string }}.startsWith({{ symbol }})) {
        console.log({{ reconstructed_symbols }}.indexOf({{ symbol }}), {{ symbol }}, String.fromCharCode({{ reconstructed_symbols }}.indexOf({{ symbol }})));
        {{ decrypted_string }} += String.fromCharCode({{ reconstructed_symbols }}.indexOf({{ symbol }}));

        {{ encrypted_string }} = {{ encrypted_string }}.substring({{ symbol }}.length);
        {{ found_symbol }} = true;
        break;
      }
    }
    if (!{{ found_symbol }} || {{ encrypted_string }}.length == 0) {
      {{ decrypting }} = false;
    }
  }

  console.log("DECRYPTED", {{ decrypted_string }});

  return {{ decrypted_string }};
}

function {{ display_fc }}({{ display_id }}, {{ display_encrypted_email }}) {
  document.getElementById({{ display_id }}).innerHTML = {{ sym_decrypt }}({{ display_encrypted_email }});
}

function {{ mailto_fc }}({{ encrypted_mailto }}) {
  window.location.href = {{ sym_decrypt }}({{ encrypted_mailto }}).replace(/\ /, "");
}

function {{ copy_fc }}({{ copy_encrypted_email }}) {
  navigator.clipboard.writeText({{ sym_decrypt }}({{ copy_encrypted_email }}));
}
